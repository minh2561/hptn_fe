export const URL = {
  home: '/',
  login: '/login',
  profile: {
    information: '/profile/information',
    bookinghistory: '/profile/bookinghistory',
  },
  admin: {
    dashboard: '/admin',
    user: '/admin/user',
    reward: '/admin/reward',
    department: '/admin/department',
    discipline: '/admin/discipline',
    benefit: '/admin/benefit',
    checkin: '/admin/checkin',
    leave: '/admin/leave',
  },
};
