import { DynamicKeyObject, ENUM_EMPLOYEE_STATUS } from 'model';

export const EMPLOYEE_STATUS: DynamicKeyObject = {
  [ENUM_EMPLOYEE_STATUS.approve]: 'Chấp thuận',
  [ENUM_EMPLOYEE_STATUS.waitingApprove]: 'Chờ chấp thuận',
  [ENUM_EMPLOYEE_STATUS.revoke]: 'Không chấp thuận',
};
