import UserForm from 'app/page/Admin/User/Form';
import { IModalForm } from 'store/appSlice';
import DepartmentForm from 'app/page/Admin/Department/Form';
import BenefitForm from 'app/page/Admin/Benefit/Form';
import RewardForm from 'app/page/Admin/Reward/Form';
import DisciplineForm from 'app/page/Admin/Discipline/Form';
import EditUserForm from 'app/page/Admin/User/EditForm';
import OtForm from 'app/page/Admin/Checkin/Form';
import LeaveForm from 'app/page/Admin/Leave/Form';

export type IModalConfigKey = 'user' | 'department' | 'benefit' | 'reward' | 'discipline' | 'editUser' | 'ot' | 'leave';

export const modalFormConfig: { [key in IModalConfigKey]: IModalForm } = {
  user: {
    title: 'Người dùng',
    apiPath: '/register',
    apiPathEdit: '/benefit/add-benefit',
    formElement: UserForm,
    isFormData: false,
  },
  editUser: {
    title: 'Người dùng',
    apiPath: '/register',
    apiPathEdit: '/employees/updateEmployee',
    formElement: EditUserForm,
    isFormData: false,
  },
  department: {
    title: 'Phòng ban',
    apiPath: '/department/addDepartment',
    apiPathEdit: '/benefit/add-benefit',
    formElement: DepartmentForm,
    isFormData: false,
  },
  benefit: {
    title: 'Quyền lợi',
    apiPath: '/benefit/add-benefit',
    apiPathEdit: '/benefit/update-benefit',
    apiDelete: '/benefit/delete-benefit',
    formElement: BenefitForm,
    isFormData: false,
  },
  reward: {
    title: 'Phần thưởng',
    apiPath: '/reward/addReward',
    apiPathEdit: '/reward/updateReward',
    formElement: RewardForm,
    isFormData: false,
  },
  discipline: {
    title: 'Kỷ luật',
    apiPath: '/discipline/addDiscipline',
    apiPathEdit: '/discipline/updateDiscipline',
    formElement: DisciplineForm,
    isFormData: false,
  },
  ot: {
    title: 'Đăng ký Onsite/Remote',
    apiPath: '/time-keeping/register-ot',
    apiPathEdit: '/discipline/updateDiscipline',
    formElement: OtForm,
    isFormData: false,
  },
  leave: {
    title: 'Đăng ký nghỉ phép',
    apiPath: '/leave/employee-leave',
    apiPathEdit: '/discipline/updateDiscipline',
    formElement: LeaveForm,
    isFormData: false,
  },
};
