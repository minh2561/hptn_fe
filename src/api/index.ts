import { DynamicKeyObject } from 'model';
import request from 'utils/request';

export const processGetQuery = async (url: string, params?: DynamicKeyObject, token?: string) => {
  return request(
    {
      url,
      method: 'GET',
      params,
    },
    false,
    token
  ).then((res) => res.data);
};

export const processPostQuery = async (url: string, data: any, isFormData: boolean = false, token?: string) => {
  return request(
    {
      url,
      method: 'POST',
      data,
    },
    isFormData,
    token
  ).then((res) => res.data);
};

export const processPutQuery = async (url: string, data: any, isFormData: boolean = false) => {
  return request(
    {
      url,
      method: 'PUT',
      data,
    },
    isFormData
  ).then((res) => res.data);
};

export const processDeleteQuery = async (url: string, token?: string) => {
  return request(
    {
      url,
      method: 'DELETE',
    },
    false,
    token
  ).then((res) => res.data);
};
