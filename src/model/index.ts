import { FormProps } from 'antd';

export interface DynamicKeyObject {
  [key: number | string]: any;
}

export interface IFormProps extends FormProps {
  imageUrl: string;
  onChangeImageUrl: (base64: string) => void;
}

export enum ENUM_USER_ROLE {
  admin = 'Đăng nhập thành công',
}
export enum ENUM_EMPLOYEE_STATUS {
  approve = 'APPROVE',
  waitingApprove = 'WAITING_APPROVE',
  revoke = 'REVOKE',
}
