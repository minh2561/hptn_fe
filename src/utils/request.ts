import axios, { AxiosRequestConfig } from 'axios';
import { loading } from './app';
import { message } from 'antd';

export const instanceAxios = axios.create();

instanceAxios.defaults.baseURL = import.meta.env.VITE_API_ENPOINT;

instanceAxios.interceptors.response.use(
  (response) => {
    setTimeout(() => {
      loading.off();
    }, 300);
    return response;
  },
  (error) => {
    loading.off();
    message.error(error.response?.data?.messageVi ?? error.response?.data?.message ?? error.message);
    return Promise.reject(error);
  }
);

export default function request(options: AxiosRequestConfig, isFormData: boolean = false, token?: string) {
  instanceAxios.defaults.headers.common['Content-Type'] = isFormData
    ? 'multipart/form-data'
    : 'application/json;charset=UTF-8';

  if (token) {
    instanceAxios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  }

  return instanceAxios(options);
}
