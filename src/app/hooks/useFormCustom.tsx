import { message, Modal } from 'antd';
import { useForm } from 'antd/es/form/Form';
import { processGetQuery, processPostQuery } from 'api';
import { DynamicKeyObject } from 'model';
import { useEffect, useState } from 'react';
import { loading, reloadPaginatedData } from 'utils/app';

interface IProps {
  apiPath: string;
  onClose: () => void;
  apiPathEdit: string;
  editedRow?: DynamicKeyObject;
  isFormData?: boolean;
}
interface Department {
  id: string;
  departmentName: string;
}
function useFormCustom(props: IProps) {
  const { apiPath, apiPathEdit, editedRow, isFormData, onClose } = props;
  const [form] = useForm();
  const [departments, setDepartments] = useState<Department[]>([]);
  useEffect(() => {
    processGetQuery('/department/getDepartment')
      .then((data) => {
        console.log(data.data);
        setDepartments(data.data);
      })
      .catch((error) => {
        console.error('Lỗi khi lấy dữ liệu:', error);
      });
  }, []);
  const handleCallApi = (formValues: any) => {
    loading.on();
    const departmentId = formValues?.departmentId;
    const updatedFormValues = {
      ...formValues,
      departmentId: departments.find((department) => department.departmentName == departmentId)?.id ?? '',
    };
    if (editedRow && editedRow.id) {
      updatedFormValues.benefitId = editedRow.id;
      updatedFormValues.id = editedRow.id;
    }
    console.log('dữ liệu cuối', updatedFormValues);
    const url = editedRow ? apiPathEdit : apiPath;
    const queryFn = processPostQuery;

    queryFn(url, updatedFormValues, isFormData).then(() => {
      onClose();
      message.success('Cập nhật thông tin thành công');
      reloadPaginatedData();
    });
  };

  const onSubmitForm = () => {
    form
      .validateFields()
      .then((values) => {
        Modal.confirm({
          title: 'Xác nhận',
          content: 'Bạn có chắc chắn muốn gửi dữ liệu này?',
          okText: 'Xác nhận',
          cancelText: 'Hủy',
          onOk() {
            handleCallApi(values);
          },
        });
      })
      .catch(() => Promise.resolve());
  };

  return { form, onSubmitForm };
}

export default useFormCustom;
