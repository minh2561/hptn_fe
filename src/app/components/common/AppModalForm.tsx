import { Modal } from 'antd';
import useFormCustom from 'app/hooks/useFormCustom';
import dayjs from 'dayjs';
import { isEmpty } from 'lodash';
import { useEffect, useState } from 'react';
import { useAppSelector } from 'store';
import { selectAppModalForm } from 'store/appSlice';
import { modalForm } from 'utils/app';

const AppModalForm = () => {
  const {
    isOpen,
    title,
    apiPath,
    apiPathEdit,
    width,
    editedRow,
    isFormData,
    formElement: FormElement,
  } = useAppSelector(selectAppModalForm);
  const [imageUrl, setImageUrl] = useState('');

  const handleCloseModal = () => {
    form.resetFields();
    setImageUrl('');
    modalForm.close();
  };

  const { form, onSubmitForm } = useFormCustom({
    apiPath,
    apiPathEdit,
    editedRow,
    isFormData,
    onClose: handleCloseModal,
  });

  useEffect(() => {
    if (!isEmpty(editedRow)) {
      form.setFieldsValue({
        ...editedRow,
        password: 'hardcode',
        rePassword: 'hardcode',
        avatar: '',
        hireDate: dayjs(editedRow.hireDate, 'DD/MM/YYYY'),
        departmentId: editedRow.departmentEntity?.departmentName || '',
        birthDay: editedRow.birthDay ? dayjs(editedRow.birthDay, 'DD/MM/YYYY') : undefined,
        benefitId: editedRow.benefitEntity?.map((benefit: any) => benefit.id) || [],
        dateIssue: editedRow.dateIssue ? dayjs(editedRow.dateIssue, 'DD/MM/YYYY') : undefined,
      });
      editedRow.avatar && setImageUrl(`${import.meta.env.VITE_API_ENPOINT}/${editedRow.avatar}`);
    }
  }, [editedRow]);

  return (
    <Modal
      title={title}
      centered
      open={isOpen}
      width={width ?? 1000}
      onOk={onSubmitForm}
      onCancel={handleCloseModal}
      maskClosable={false}
    >
      {FormElement && <FormElement form={form} imageUrl={imageUrl} onChangeImageUrl={setImageUrl} />}
    </Modal>
  );
};

export default AppModalForm;
