import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { Button, message, Space, Modal, Form, Input, Row, Col } from 'antd';
import { processPostQuery } from 'api';
import { DynamicKeyObject } from 'model';
import { confirmation } from 'utils/app';
import { useState } from 'react';

interface IProps {
  row: DynamicKeyObject;
  employeeCode: string;
  apiDelete?: string;
  onUpdate?: () => void;
}

function TableCertificateAction(props: Readonly<IProps>) {
  const { row, apiDelete, employeeCode, onUpdate } = props;
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [form] = Form.useForm();

  const showModal = () => {
    form.setFieldsValue({
      row,
    });
    setIsModalVisible(true);
  };

  const handleEditRow = () => {
    console.log(row);
    showModal();
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    form.resetFields();
  };

  const handleOk = () => {
    form.validateFields().then((values) => {
      const updatedFormValues = {
        ...values,
        employeeCode: employeeCode,
        id: row.id,
      };
      processPostQuery('/family/updatePerson', updatedFormValues)
        .then((response) => {
          if (response.messageCode == 'Cập nhật thành công') {
            message.success('Cập nhật thành viên gia đình thành công');
            setIsModalVisible(false);
            form.resetFields();
            if (onUpdate) onUpdate();
          } else {
            message.error('Có lỗi xảy ra, vui lòng thử lại sau');
          }
        })
        .catch(() => {
          message.error('Có lỗi xảy ra, vui lòng thử lại sau');
        });
    });
  };

  const handleDeleteRow = () => {
    confirmation({
      type: 'multi',
      title: 'Xác nhận',
      message: 'Bạn chắc chắn muốn xoá bản ghi này?',
      onSubmit: () => {
        processPostQuery(`${apiDelete}/${row.id}`, undefined).then(() => {
          message.success('Xoá bản ghi thành công');
          if (onUpdate) onUpdate();
        });
      },
    });
  };

  return (
    <Space size="middle" className="flex gap-1">
      <Button
        type="text"
        className="hover:!bg-[#fff6da]"
        icon={<EditOutlined className="text-warning" />}
        size="middle"
        onClick={handleEditRow}
      />
      <Button type="text" danger icon={<DeleteOutlined />} size="middle" onClick={handleDeleteRow} />
      <Modal
        title="Sửa thành viên gia đình"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        okText="Lưu"
        cancelText="Hủy"
      >
        <Form form={form} layout="vertical">
          <Row>
            <Col span={24}>
              <Form.Item
                label="Tên chứng chỉ"
                name="nameCertificate"
                rules={[{ required: true, message: 'Vui lòng nhập tên chứng chỉ' }]}
              >
                <Input />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item
                label="Mô tả chứng chỉ"
                name="description"
                rules={[{ required: true, message: 'Vui lòng nhập mô tả chứng chỉr' }]}
              >
                <Input />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Modal>
    </Space>
  );
}

export default TableCertificateAction;
