import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { Button, Space, message } from 'antd';
import { processPostQuery } from 'api';
import { modalFormConfig } from 'constants/modalForm';
import dayjs from 'dayjs';
import { DynamicKeyObject } from 'model';
import { confirmation, modalForm, reloadPaginatedData } from 'utils/app';

interface IProps {
  row: DynamicKeyObject;
  apiPath: string;
  apiDelete?: string;
}

function UserAction(props: Readonly<IProps>) {
  const { row, apiDelete } = props;
  const handleEditRow = () => {
    const departmentName = row.departmentName;
    const hireDate = dayjs(row.hireDate, 'DD/MM/YYYY').format('YYYY-MM-DD');
    modalForm.open({
      ...modalFormConfig.editUser,
      editedRow: {
        ...row,
        departmentId: departmentName,
      },
    });
  };

  const handleDeleteRow = () => {
    const user = row;
    const url = `${apiDelete}/${user.code}`;
    confirmation({
      type: 'multi',
      title: 'Xác nhận',
      message: 'Bạn chắc chắn muốn xoá bản ghi này?',
      onSubmit: () => {
        processPostQuery(url, user).then(() => {
          message.success('Xoá bản ghi thành công');
          reloadPaginatedData();
        });
      },
    });
  };

  return (
    <Space size="middle" className="flex gap-1" onClick={(e) => e.stopPropagation()}>
      <Button
        type="text"
        className="hover:!bg-[#fff6da]"
        icon={<EditOutlined className="text-warning" />}
        size="middle"
        onClick={handleEditRow}
      />
      <Button type="text" danger icon={<DeleteOutlined />} size="middle" onClick={handleDeleteRow} />
    </Space>
  );
}

export default UserAction;
