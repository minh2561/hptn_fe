import {
  ApartmentOutlined,
  AuditOutlined,
  CalendarOutlined,
  HomeOutlined,
  LineChartOutlined,
  LogoutOutlined,
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  MoneyCollectOutlined,
  UserOutlined,
} from '@ant-design/icons';
import { Button, Image, Layout, Menu, theme } from 'antd';
import { URL } from 'constants/url';
import { useState } from 'react';
import { Link, Navigate, useNavigate } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from 'store';
import { actionLogout, selectUser } from 'store/authSlice';
import { getPageName } from 'utils/app';

const { Header, Sider, Content } = Layout;

interface IProps {
  children: JSX.Element;
}

const menu = [
  {
    key: 'admin',
    label: <Link to={URL.admin.dashboard}>Trang chủ</Link>,
    icon: <HomeOutlined />,
  },
  {
    key: 'user',
    icon: <UserOutlined />,
    label: <Link to={URL.admin.user}>Nhân viên</Link>,
  },
  {
    key: 'department',
    label: <Link to={URL.admin.department}>Phòng ban</Link>,
    icon: <ApartmentOutlined />,
  },
  {
    key: 'checkin',
    label: <Link to={URL.admin.checkin}>Chấm công</Link>,
    icon: <CalendarOutlined />,
  },
  {
    key: 'benefit',
    label: <Link to={URL.admin.benefit}>Quyền lợi</Link>,
    icon: <LineChartOutlined />,
  },
  {
    key: 'reward',
    label: <Link to={URL.admin.reward}>Khen thưởng</Link>,
    icon: <MoneyCollectOutlined />,
  },
  {
    key: 'discipline',
    label: <Link to={URL.admin.discipline}>Kỉ luật</Link>,
    icon: <AuditOutlined />,
  },
  {
    key: 'leave',
    label: <Link to={URL.admin.leave}>Nghỉ phép</Link>,
    icon: <AuditOutlined />,
  },
];

const AdminLayout = (props: IProps) => {
  const { children } = props;
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const [isCollapse, setIsCollapse] = useState(false);
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();
  const user = useAppSelector(selectUser);
  const token = sessionStorage.getItem('token');
  return (
    <>
      {token == null ? (
        <Navigate to={URL.login} />
      ) : (
        <Layout>
          <Sider
            trigger={null}
            collapsible
            collapsed={isCollapse}
            theme="light"
            className="border-0 border-r-[1px] border-solid border-r-[#eeeeee]"
          >
            <div className="p-[5px]">
              <Link to={URL.home}>
                <Image src="https://www.tlu.edu.vn/Portals/0/2014/Logo-WRU.png" preview={false} />
              </Link>
            </div>
            <Menu mode="inline" defaultSelectedKeys={[getPageName()]} items={menu} />
          </Sider>
          <Layout>
            <Header
              className="flex justify-between items-center pr-5 pl-0"
              style={{
                background: colorBgContainer,
              }}
            >
              <Button
                type="text"
                icon={isCollapse ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
                onClick={() => setIsCollapse(!isCollapse)}
                style={{
                  width: 64,
                  height: 64,
                }}
                size="large"
              />
              <div className="flex items-center gap-3">
                <p className="font-bold text-base">Hello, </p>
                <LogoutOutlined
                  title="Logout"
                  className="text-xl font-extrabold cursor-pointer hover:text-info"
                  onClick={() => {
                    dispatch(actionLogout());
                    navigate(URL.home);
                  }}
                />
              </div>
            </Header>
            <Content
              style={{
                margin: '24px 16px',
                padding: 24,
                height: 'calc(100vh - 48px - 64px)',
                background: colorBgContainer,
                borderRadius: borderRadiusLG,
                overflow: 'auto',
              }}
            >
              {children}
            </Content>
          </Layout>
        </Layout>
      )}
    </>
  );
};

export default AdminLayout;
