import { message } from 'antd';
import AdminLayout from 'app/layout/Admin/index';
import Dashboard from 'app/page/Admin/Dashboard';
import User from 'app/page/Admin/User';
import NotFound from 'app/page/NotFound';
import { URL } from 'constants/url';
import { lazy, ReactElement, Suspense, useEffect } from 'react';
import { Route, Routes, useLocation } from 'react-router-dom';
import Department from 'app/page/Admin/Department';
import BenefitMangament from 'app/page/Admin/Benefit';
import CheckinMangament from 'app/page/Admin/Checkin';
import Reward from 'app/page/Admin/Reward';
import Discipline from 'app/page/Admin/Discipline';
import Leave from 'app/page/Admin/Leave';

const CUSTOMER_LAYOUT = 'customer';
const ADMIN_LAYOUT = 'admin';
const USER_LAYOUT = 'user';
const NONE_LAYOUT = 'none';

const Login = lazy(() => import('app/page/Auth'));

interface ItemType {
  key: string;
  components: ReactElement;
  layout: string;
}

const customerItems: ItemType[] = [
  {
    key: URL.home,
    components: <Dashboard />,
    layout: ADMIN_LAYOUT,
  },
];

const adminItems: ItemType[] = [
  {
    key: URL.admin.dashboard,
    components: <Dashboard />,
    layout: ADMIN_LAYOUT,
  },
  {
    key: URL.admin.user,
    components: <User />,
    layout: ADMIN_LAYOUT,
  },
  {
    key: URL.admin.reward,
    components: <Reward />,
    layout: ADMIN_LAYOUT,
  },
  {
    key: URL.admin.checkin,
    components: <CheckinMangament />,
    layout: ADMIN_LAYOUT,
  },
  {
    key: URL.admin.department,
    components: <Department />,
    layout: ADMIN_LAYOUT,
  },
  {
    key: URL.admin.discipline,
    components: <Discipline />,
    layout: ADMIN_LAYOUT,
  },
  {
    key: URL.admin.benefit,
    components: <BenefitMangament />,
    layout: ADMIN_LAYOUT,
  },
  {
    key: URL.admin.leave,
    components: <Leave />,
    layout: ADMIN_LAYOUT,
  },
];

const sharedItems: ItemType[] = [
  {
    key: URL.login,
    components: <Login />,
    layout: NONE_LAYOUT,
  },
  {
    key: '*',
    components: <NotFound />,
    layout: NONE_LAYOUT,
  },
];

message.config({
  duration: 5,
  maxCount: 3,
});

export default function Routers() {
  const items = customerItems.concat(adminItems, sharedItems);
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return (
    <Routes>
      {items.map((item) => {
        let element = item.components;

        element = <Suspense fallback={null}>{element}</Suspense>;

        if (item.layout === ADMIN_LAYOUT) {
          element = <AdminLayout>{element}</AdminLayout>;
        }

        return <Route key={item.key} path={item.key} element={element} />;
      })}
    </Routes>
  );
}
