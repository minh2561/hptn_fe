import { URL } from 'constants/url';
import { Navigate } from 'react-router-dom';
import { useAppSelector } from 'store';
import { selectUser } from 'store/authSlice';
import Login from './Login';

function Auth() {
  const user = useAppSelector(selectUser);
  const token = sessionStorage.getItem('token');

  return (
    <>
      {token !== null ? (
        <Navigate to={URL.admin.dashboard} />
      ) : (
        <div className="h-[100vh] flex justify-center items-center w-full bg-[#f5f5dc]">
          <Login />
        </div>
      )}
    </>
  );
}

export default Auth;
