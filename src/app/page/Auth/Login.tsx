import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Form, Input } from 'antd';
import { useAppDispatch } from 'store';
import { IPayloadLogin, actionLogin } from 'store/authSlice';

function Login() {
  const dispatch = useAppDispatch();
  const handleLogin = (values: IPayloadLogin) => {
    dispatch(actionLogin(values));
  };

  return (
    <div className="login-container">
      <h1 className="login-title uppercase">Đăng nhập</h1>
      <Form
        name="normal_login"
        className="w-[400px]"
        initialValues={{ remember: true }}
        onFinish={handleLogin}
        labelCol={{ span: 24 }}
        wrapperCol={{ span: 24 }}
      >
        <Form.Item
          label="Tài khoản"
          name="email"
          rules={[
            { required: true, message: 'Vui lòng nhập Email của bạn!' },
            { type: 'email', message: 'Email không hợp lệ!' },
          ]}
        >
          <Input
            size="large"
            prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="Nhập email của bạn"
            allowClear
          />
        </Form.Item>
        <Form.Item
          label="Mật khẩu"
          name="password"
          rules={[{ required: true, message: 'Vui lòng nhập Mật khẩu của bạn!' }]}
        >
          <Input
            size="large"
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Nhập mật khẩu của bạn"
            allowClear
          />
        </Form.Item>
        <Form.Item>
          <Button size="large" htmlType="submit" className="w-full mb-1 login-button">
            Đăng nhập
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}

export default Login;
