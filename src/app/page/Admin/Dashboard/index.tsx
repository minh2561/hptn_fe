import { Row, Col, Input } from 'antd';
import { processGetQuery } from 'api';
import { EMPLOYEE_STATUS } from 'constants/app';
import { useEffect, useState } from 'react';

const Dashboard = () => {
  const [userInfor, setUserInfor] = useState<any>([]);
  const token = sessionStorage.getItem('token');
  useEffect(() => {
    if (token) {
      processGetQuery('/employees/my-profile', undefined, token)
        .then((data) => {
          setUserInfor(data.data);
        })
        .catch((error) => {
          console.error('Lỗi khi lấy dữ liệu:', error);
        });
    }
  }, [token]);
  console.log('DashBoard', userInfor);
  const gender = userInfor?.gender ? 'Nam' : 'Nữ';
  const hasManager = userInfor?.hasManager ? 'Quản lý' : 'Nhân viên';
  return (
    <div>
      <h2>TỔNG QUAN NHÂN VIÊN </h2>
      <div className="w-[1400px] mx-auto my-0 ">
        <Row className="p-4" gutter={[4, 16]}>
          <Col span={12}>
            <Row>
              <Col span={6} className="mb-2 text-2xl">
                Họ và tên:
              </Col>
              <Col span={18}>
                <Input className="w-2/3 ml-1 text-2xl" value={userInfor?.name} disabled />
              </Col>
            </Row>
          </Col>
          <Col span={12}>
            <Row>
              <Col span={6} className="mb-2 text-2xl">
                Giới tính:
              </Col>
              <Col span={18}>
                <Input className="w-2/3 ml-1 text-2xl" style={{ fontSize: '18px' }} value={gender} disabled />
              </Col>
            </Row>
          </Col>
          <Col span={12}>
            <Row>
              <Col span={6} className="mb-2 text-2xl">
                Ngày sinh:
              </Col>
              <Col span={18}>
                <Input
                  className="w-2/3 ml-1 text-2xl"
                  style={{ fontSize: '18px' }}
                  value={userInfor?.birthDay}
                  disabled
                />
              </Col>
            </Row>
          </Col>
          <Col span={12}>
            <Row>
              <Col span={6} className="mb-2 text-2xl">
                Quốc tịch:
              </Col>
              <Col span={18}>
                <Input
                  className="w-2/3 ml-1 text-2xl"
                  style={{ fontSize: '18px' }}
                  value={userInfor?.nationality}
                  disabled
                />
              </Col>
            </Row>
          </Col>
          <Col span={12}>
            <Row>
              <Col span={6} className="mb-2 text-2xl">
                Vị trí:
              </Col>
              <Col span={18}>
                <Input
                  className="w-2/3 ml-1 text-2xl"
                  style={{ fontSize: '18px' }}
                  value={userInfor?.position}
                  disabled
                />
              </Col>
            </Row>
          </Col>
          <Col span={12}>
            <Row>
              <Col span={6} className="mb-2 text-2xl">
                Phòng ban:
              </Col>
              <Col span={18}>
                <Input
                  className="w-2/3 ml-1 text-2xl"
                  style={{ fontSize: '18px' }}
                  value={userInfor?.departmentEntity?.departmentName || ''}
                  disabled
                />
              </Col>
            </Row>
          </Col>
          <Col span={12}>
            <Row>
              <Col span={6} className="mb-2 text-2xl">
                Email:
              </Col>
              <Col span={18}>
                <Input className="w-2/3 ml-1 text-2xl" style={{ fontSize: '18px' }} value={userInfor?.email} disabled />
              </Col>
            </Row>
          </Col>
          <Col span={12}>
            <Row>
              <Col span={6} className="mb-2 text-2xl">
                Ngày ký hợp đồng:
              </Col>
              <Col span={18}>
                <Input
                  className="w-2/3 ml-1 text-2xl"
                  style={{ fontSize: '18px' }}
                  value={userInfor?.hireDate}
                  disabled
                />
              </Col>
            </Row>
          </Col>
          <Col span={12}>
            <Row>
              <Col span={6} className="mb-2 text-2xl">
                Chức vụ:
              </Col>
              <Col span={18}>
                <Input className="w-2/3 ml-1 text-2xl" style={{ fontSize: '18px' }} value={hasManager} disabled />
              </Col>
            </Row>
          </Col>
          <Col span={12}>
            <Row>
              <Col span={6} className="mb-2 text-2xl">
                Trạng thái:
              </Col>
              <Col span={18}>
                <Input
                  className="w-2/3 ml-1 text-2xl"
                  style={{ fontSize: '18px' }}
                  value={EMPLOYEE_STATUS[userInfor?.typeStatus]}
                  disabled
                />
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default Dashboard;
