import { Col, DatePicker, DatePickerProps, Form, Input, Row } from 'antd';
import { processGetQuery } from 'api';
import { IFormProps } from 'model';
import { useEffect, useState } from 'react';

const LeaveForm = (props: IFormProps) => {
  const { form } = props;
  const onChange: DatePickerProps['onChange'] = (date, dateString) => {
    console.log(date);
  };
  const [userInfor, setUserInfor] = useState<any>([]);
  useEffect(() => {
    processGetQuery('/employees/my-profile')
      .then((data) => {
        setUserInfor(data.data);
      })
      .catch((error) => {
        console.error('Lỗi khi lấy dữ liệu:', error);
      });
  }, []);
  console.log(userInfor.code);
  return (
    <Form form={form} initialValues={{ remember: true }} labelCol={{ span: 24 }} wrapperCol={{ span: 24 }}>
      <Row gutter={[10, 0]}>
        <Col span={24}>
          <Form.Item label="Lý do nghỉ phép" name="reason">
            <Input size="large" placeholder="Nhập lý do nghỉ phép" allowClear />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item label="Bắt đầu từ ngày" name="startTime">
            <DatePicker className="w-full" onChange={onChange} size="large" placeholder="Chọn ngày" />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item label="Kết thúc ngày" name="endTime">
            <DatePicker className="w-full" onChange={onChange} size="large" placeholder="Chọn ngày" />
          </Form.Item>
        </Col>
        <Form.Item label="" name="emplyeeCode" className="" initialValue={userInfor.code}></Form.Item>
      </Row>
    </Form>
  );
};

export default LeaveForm;
