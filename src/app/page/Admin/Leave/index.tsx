import type { TableProps } from 'antd';
import { Button, Col, Row, Table } from 'antd';
import AppPagination from 'app/components/common/AppPagination';
import { DynamicKeyObject } from 'model';
import { useState } from 'react';
import Search from 'antd/es/input/Search';
import { modalForm } from 'utils/app';
import { modalFormConfig } from 'constants/modalForm';

const Leave = () => {
  const [data, setData] = useState<DynamicKeyObject>({});
  const role = sessionStorage.getItem('role');
  const columns: TableProps<any>['columns'] = [
    {
      title: 'Mã xin nghỉ',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'Lý do xin nghỉ',
      dataIndex: 'reason',
      key: 'reason',
    },
    {
      title: 'Ngày bắt đầu',
      dataIndex: 'endTime',
      key: 'endTime',
    },
    {
      title: 'Ngày kết thúc',
      dataIndex: 'startTime',
      key: 'startTime',
    },
  ];
  return (
    <>
      <h2 className="uppercase">Quản lý nghỉ phép </h2>
      <Row justify="end" gutter={[10, 0]}>
        <Col span={4}>
          <Search placeholder="Tìm kiếm" />
        </Col>
        <Col span={3}>
          <Button type="primary" className="w-full mb-5" onClick={() => modalForm.open(modalFormConfig.leave)}>
            Xin nghỉ+
          </Button>
        </Col>
      </Row>
      <Table columns={columns} dataSource={data.data} pagination={false} />
      <AppPagination onChangeDataTable={setData} apiPath={'/leave/get-employee-leave'} />
    </>
  );
};

export default Leave;
