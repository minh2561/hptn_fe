import { Col, Modal, Row, Table, TableProps } from 'antd';
import { DynamicKeyObject } from 'model';

interface IProps {
  isOpen: boolean;
  handleCloseModal: () => void;
  handleSubmitForm: () => void;
  row: DynamicKeyObject;
}
const columnsCCCD: TableProps<any>['columns'] = [
  {
    title: 'Loại thẻ',
    dataIndex: 'typeCccd',
    key: 'typeCccd',
  },
  {
    title: 'Số thẻ',
    dataIndex: 'cccd',
    key: 'cccd',
  },
  {
    title: 'Ngày cấp',
    dataIndex: 'dateIssue',
    key: 'dateIssue',
  },
  {
    title: 'Nơi cấp',
    dataIndex: 'placeIssue',
    key: 'placeIssue',
  },
];
const columnsBank: TableProps<any>['columns'] = [
  {
    title: 'Ngân hàng',
    dataIndex: 'bankName',
    key: 'bankName',
  },
  {
    title: 'Số tài khoản',
    dataIndex: 'bankNumber',
    key: 'bankNumber',
  },
  {
    title: 'Chi nhánh',
    dataIndex: 'salary',
    key: 'salary',
  },
];
const columnsFamily: TableProps<any>['columns'] = [
  {
    title: 'Họ và tên',
    dataIndex: 'fullName',
    key: 'fullName',
  },
  {
    title: 'Quan hệ với nhân viên',
    dataIndex: 'relationship',
    key: 'relationship',
  },
  {
    title: 'Ngày sinh',
    dataIndex: 'birthDay',
    key: 'birthDay',
  },
  {
    title: 'Giới tính',
    dataIndex: 'gender',
    key: 'gender',
    render: (gender) => (gender ? 'Nam' : 'Nữ'),
  },
];
const columnsCertificate: TableProps<any>['columns'] = [
  {
    title: 'Tên chứng chỉ',
    dataIndex: 'nameCertificate',
    key: 'nameCertificate',
  },
  {
    title: 'Thông tin chứng chỉ',
    dataIndex: 'description',
    key: 'description',
  },
];
function DetailUser(props: Readonly<IProps>) {
  const { isOpen, row, handleCloseModal, handleSubmitForm } = props;
  console.log('row', row);
  const data = [row];
  const gender = row.gender ? 'Nam' : 'Nữ';
  return (
    <Modal
      title={<p className="text-2xl">Thông tin nhân viên</p>}
      centered
      open={isOpen}
      width={1200}
      onOk={handleSubmitForm}
      onCancel={handleCloseModal}
      maskClosable={false}
      className="detailUser"
    >
      <div className="border-solid border-2 rounded  border-white bg-white my-4">
        <Row className="p-4">
          <Col span={24}>
            <p className="mb-2 font-bold text-2xl">{row.name}</p>
          </Col>
          <Col span={12}>
            <p>Ngày bắt đầu làm việc: {row.hireDate}</p>
            <p>Loại hợp đồng:</p>
            <p>Thời gian làm việc:</p>
            <p>Tình trạng làm việc:</p>
            <p>Thời gian bắt đầu đi làm:</p>
            <p>Số năm làm việc: </p>
          </Col>
          <Col span={12}>
            <p>Số điện thoại: {row.phoneNumber}</p>
            <p>Email: {row.email}</p>
            <p>Ngày sinh: {row.birthDay}</p>
            <p>Giới tính: {gender}</p>
            <p>Địa chỉ: </p>
            <p>Ngày kết thúc làm việc:</p>
          </Col>
        </Row>
      </div>
      <Row>
        <Col span={12}>
          <Col span={24}>
            <div className="border-solid border-2 rounded  border-white bg-white my-4 mr-8 p-4">
              <p className="mb-2 font-bold text-base">Thông tin địa chỉ</p>
              <p>
                <span className="font-bold">Quốc tịch:</span> {row.nationality}
              </p>
              <div className="py-2 border-t">
                <p className="font-bold">Nguyên quán</p>
                <div className="pl-2">
                  <p>Quận/Huyện: {row.districtDomicile}</p>
                  <p>Tỉnh/Thành phố:{row.provinceDomicile}</p>
                </div>
              </div>
              <div className="py-2 border-t">
                <p className="font-bold">Hộ khẩu thường trú</p>
                <div className="pl-2">
                  <p>Địa chỉ chi tiết: {row.detailAddressPermanentResidence}</p>
                  <p>Quận/Huyện: {row.districtPermanentResidence}</p>
                  <p>Tỉnh/Thành phố: {row.provincePermanentResidence}</p>
                </div>
              </div>
              <div className="py-2 border-t">
                <p className="font-bold">Nơi ở hiện tại</p>
                <div className="pl-2">
                  <p>Địa chỉ chi tiết: {row.detailAddressResidence}</p>
                  <p>Phường/Xã:{row.districtResidence}</p>
                  <p>Quận/Huyện: {row.districtResidence}</p>
                  <p>Tỉnh/Thành phố: {row.provinceResidence}</p>
                </div>
              </div>
            </div>
          </Col>
          <Col span={24}>
            <div className="border-solid border-2 rounded  border-white bg-white my-4 mr-8 p-4">
              <p className="mb-2 font-bold text-base">Danh sách hồ sơ lưu trữ</p>
              <p>Bằng tốt nghiệp: {row.graduaDegree}</p>
              <p>Số bảo hiểm: {row.socialInsurance}</p>
              <p>Chứng chỉ</p>
              <Table
                columns={columnsCertificate}
                dataSource={row.certificateEntities}
                pagination={false}
                style={{ width: '100%' }}
              />
            </div>
          </Col>
          <Col span={24}>
            <div className="border-solid border-2 rounded  border-white bg-white my-4 mr-8 p-4">
              <p className="mb-2 font-bold text-base">Thông tin lương</p>
              <p>Mức lương: {row.salary}</p>
            </div>
          </Col>
        </Col>
        <Col span={12}>
          <Row className="w-full my-4">
            <Col span={24}>
              <div className="border-solid border-2 rounded  border-white bg-white p-4">
                <p className="mb-2 font-bold text-base">Thông tin CMND/CCCD</p>
                <Table columns={columnsCCCD} dataSource={data} pagination={false} style={{ width: '100%' }} />
              </div>
            </Col>
          </Row>
          <Row className="w-full my-4">
            <Col span={24}>
              <div className="border-solid border-2 rounded  border-white bg-white p-4">
                <p className="mb-2 font-bold text-base">Thông tin ngân hàng</p>
                <Table columns={columnsBank} dataSource={data} pagination={false} />
              </div>
            </Col>
          </Row>
          <Row className="w-full my-4">
            <Col span={24}>
              <div className="border-solid border-2 rounded  border-white bg-white p-4">
                <p className="mb-2 font-bold text-base">Thông tin gia đình</p>
                <Table columns={columnsFamily} dataSource={row.familyEntities} pagination={false} />
              </div>
            </Col>
          </Row>
          <Row className="w-full my-4">
            <Col span={24}>
              <div className="border-solid border-2 rounded  border-white bg-white p-4">
                <p className="font-bold">Thông tin nhóm và vai trò</p>
                <p>Nhóm: {row.departmentEntity.departmentName}</p>
                <p>Vai trò: {row.position}</p>{' '}
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
    </Modal>
  );
}

export default DetailUser;
