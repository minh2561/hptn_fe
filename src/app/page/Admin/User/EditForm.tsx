import { UserOutlined } from '@ant-design/icons';
import { Button, Col, DatePicker, Form, Input, message, Modal, Row, Select, Table, TableProps } from 'antd';
import { DynamicKeyObject, IFormProps } from 'model';
import { useState, useEffect } from 'react';
import { useAppSelector } from 'store';
import { selectAppModalForm } from 'store/appSlice';
import { processGetQuery, processPostQuery } from 'api';
import TableFamilyAction from 'app/components/custom/TableFamilyAction';
import TableCertificateAction from 'app/components/custom/TableCertificateAction';

interface Department {
  id: string;
  departmentName: string;
}

const EditUserForm = (props: IFormProps) => {
  const { form } = props;
  const { Option } = Select;
  const [isLoading, setIsLoading] = useState(false);
  const { editedRow } = useAppSelector(selectAppModalForm);
  const [departments, setDepartments] = useState<Department[]>([]);
  const [benefits, setBenefits] = useState<any>([]);
  const [isFamilyModalVisible, setIsFamilyModalVisible] = useState(false);
  const [isCertificateModalVisible, setIsCertificateModalVisible] = useState(false);
  const [familyForm] = Form.useForm();
  const [certificateForm] = Form.useForm();
  const [familyData, setFamilyData] = useState(editedRow?.familyEntities || []);
  const [certificateData, setCertificateData] = useState(editedRow?.certificateEntities || []);
  const columnsFamily: TableProps<any>['columns'] = [
    {
      title: 'Họ và tên',
      dataIndex: 'fullName',
      key: 'fullName',
    },
    {
      title: 'Quan hệ với nhân viên',
      dataIndex: 'relationship',
      key: 'relationship',
    },
    {
      title: 'Ngày sinh',
      dataIndex: 'birthDay',
      key: 'birthDay',
    },
    {
      title: 'Giới tính',
      dataIndex: 'gender',
      key: 'gender',
      render: (gender) => (gender ? 'Nam' : 'Nữ'),
    },
    {
      title: 'Hành động',
      key: 'action',
      render: (_, record) => (
        <TableFamilyAction
          row={record}
          employeeCode={editedRow?.code}
          apiDelete={'/family/delPerson'}
          onUpdate={fetchData}
        />
      ),
    },
  ];
  const columnsCertificate: TableProps<any>['columns'] = [
    {
      title: 'Tên chứng chỉ',
      dataIndex: 'nameCertificate',
      key: 'nameCertificate',
    },
    {
      title: 'Thông tin chứng chỉ',
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: 'Hành động',
      key: 'action',
      render: (_, record) => (
        <TableCertificateAction
          row={record}
          employeeCode={editedRow?.code}
          apiDelete={'/certificate/delete-certificate'}
          onUpdate={fetchData}
        />
      ),
    },
  ];
  useEffect(() => {
    processGetQuery('/department/getDepartment')
      .then((data) => {
        console.log(data.data);
        setDepartments(data.data);
        setIsLoading(false);
      })
      .catch((error) => {
        console.error('Lỗi khi lấy dữ liệu:', error);
        setIsLoading(false);
      });
    processGetQuery('/benefit/get-benefit').then((data) => {
      const nextBenefits = data.data.map((benefit: DynamicKeyObject) => ({
        value: benefit.id,
        label: benefit.benefitType,
      }));
      setBenefits(nextBenefits);
    });
  }, []);
  useEffect(() => {
    setFamilyData(editedRow?.familyEntities || []);
    setCertificateData(editedRow?.certificateEntities || []);
  }, [editedRow]);
  const fetchData = async () => {
    try {
      const response = await processGetQuery(`/employees/getEmployee`);
      const employees = response.data;
      const matchedEmployee = employees.find((employee: DynamicKeyObject) => employee.code === editedRow?.code);
      console.log(response.data);
      if (matchedEmployee) {
        setFamilyData(matchedEmployee.familyEntities);
        setCertificateData(matchedEmployee.certificateEntities);
      } else {
        console.error('Không tìm thấy nhân viên với mã code:', editedRow?.code);
      }
    } catch (error) {
      console.error('Lỗi khi lấy dữ liệu gia đình:', error);
    }
  };

  const handleAddFamilyMember = () => {
    familyForm
      .validateFields()
      .then((values) => {
        const updatedFormValues = {
          ...values,
          employeeCode: editedRow?.code,
        };
        console.log('Validate Failed:', editedRow?.code);
        processPostQuery('/family/addPerson', updatedFormValues)
          .then((response) => {
            if (response.messageCode == 'Thêm thành công') {
              console.log('Thêm thành viên thành công:', response.data);
              setIsFamilyModalVisible(false);
              familyForm.resetFields();
              fetchData();
            } else {
              message.error('Có lỗi xảy ra, vui lòng thử lại sau');
            }
          })
          .catch((error) => {
            console.error('Lỗi khi thêm thành viên:', error);
          });
      })
      .catch((errorInfo) => {
        console.log('Validate Failed:', errorInfo);
      });
  };
  const handleAddCertificate = () => {
    certificateForm
      .validateFields()
      .then((values) => {
        const updatedFormValues = {
          ...values,
          employeeCode: editedRow?.code,
        };
        processPostQuery('/certificate/add-certificate', updatedFormValues)
          .then((response) => {
            if (response.messageCode == 'Thêm thành công') {
              console.log('Thêm chứng chỉ thành công:', response.data);
              setIsCertificateModalVisible(false);
              certificateForm.resetFields();
              fetchData();
            } else {
              message.error('Có lỗi xảy ra, vui lòng thử lại sau');
            }
          })
          .catch((error) => {
            console.error('Lỗi khi thêm chứng chỉ:', error);
          });
      })
      .catch((errorInfo) => {
        console.log('Validate Failed:', errorInfo);
      });
  };
  console.log('data', editedRow);
  return (
    <>
      <Form form={form} initialValues={{ remember: true }} labelCol={{ span: 24 }} wrapperCol={{ span: 24 }}>
        <Row gutter={[10, 0]}>
          <Col span={12}>
            <Form.Item
              label="Tên nhân viên"
              name="name"
              rules={[{ required: true, message: 'Please input your name!' }]}
            >
              <Input
                size="large"
                prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder="Nhập tên nhân viên mới"
                allowClear
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Tên vị trí" name="position">
              <Input size="large" placeholder="Nhập tên vị trí" allowClear />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Lương" name="salary">
              <Input size="large" allowClear />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Mật khẩu" name="code">
              <Input size="large" allowClear />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label="Phòng ban"
              name="departmentId"
              rules={[{ required: true, message: 'Please select user role!' }]}
            >
              <Select placeholder="Chọn phòng ban" loading={isLoading} size="large" style={{ width: '100%' }}>
                {departments.map((dept) => (
                  <Option key={dept.id} value={dept.departmentName}>
                    {dept.departmentName}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Ngày kí hợp đồng" name="hireDate">
              <DatePicker className="w-full" size="large" placeholder="Chọn ngày" format="DD/MM/YYYY" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label="Quyền lợi"
              name="benefitId"
              rules={[{ required: true, message: 'Vui lòng chọn quyền lợi' }]}
            >
              <Select
                size="large"
                mode="multiple"
                placeholder="Chọn quyền lợi"
                style={{ flex: 1 }}
                options={benefits}
                showSearch={false}
                allowClear
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Giới tính" name="gender" rules={[{ required: true, message: 'Vui lòng chọn giới tính' }]}>
              <Select>
                <Select.Option value={true}>Nam</Select.Option>
                <Select.Option value={false}>Nữ</Select.Option>
              </Select>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Ngày sinh" name="birthDay">
              <DatePicker className="w-full" size="large" placeholder="Chọn ngày" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Quốc tịch" name="nationality">
              <Input size="large" allowClear />
            </Form.Item>
          </Col>
          <Col span={24}>
            <p className="mb-2 font-bold text-base">Nguyên quán</p>
            <Row gutter={[10, 0]}>
              <Col span={12}>
                <Form.Item label="Quận/Huyện" name="districtDomicile">
                  <Input size="large" allowClear />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="Tỉnh/Thành phố" name="provinceDomicile">
                  <Input size="large" allowClear />
                </Form.Item>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            <p className="mb-2 font-bold text-base">Hộ khẩu thường trú</p>
            <Row gutter={[10, 0]}>
              <Col span={8}>
                <Form.Item label="Địa chỉ chi tiết:" name="detailAddressPermanentResidence">
                  <Input size="large" allowClear />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="Quận/Huyện:" name="districtPermanentResidence">
                  <Input size="large" allowClear />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="Tỉnh/Thành phố:" name="provincePermanentResidence">
                  <Input size="large" allowClear />
                </Form.Item>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            <p className="mb-2 font-bold text-base">Nơi ở hiện tại</p>
            <Row gutter={[10, 0]}>
              <Col span={8}>
                <Form.Item label="Địa chỉ chi tiết:" name="detailAddressResidence">
                  <Input size="large" allowClear />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="Quận/Huyện:" name="districtResidence">
                  <Input size="large" allowClear />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="Tỉnh/Thành phố:" name="provinceResidence">
                  <Input size="large" allowClear />
                </Form.Item>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            <p className="mb-2 font-bold text-base">Thông tin căn cước công dân</p>

            <Row gutter={[10, 0]}>
              <Col span={6}>
                <Form.Item label="Loại thẻ" name="typeCccd">
                  <Input size="large" allowClear />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item label="Số thẻ" name="cccd">
                  <Input size="large" allowClear />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item label="Ngày cấp" name="dateIssue">
                  <DatePicker className="w-full" size="large" placeholder="Chọn ngày" format="DD/MM/YYYY" />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item label="Nơi cấp" name="placeIssue">
                  <Input size="large" allowClear />
                </Form.Item>
              </Col>
            </Row>
          </Col>
          <Col span={12}>
            <Form.Item label="Bằng tốt nghiệp" name="graduaDegree">
              <Input size="large" allowClear />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Số bảo hiểm" name="socialInsurance">
              <Input size="large" allowClear />
            </Form.Item>
          </Col>
          <Col span={24}>
            <p className="mb-2 font-bold text-base">Thông tin ngần hàng</p>
            <Row gutter={[10, 0]}>
              <Col span={12}>
                <Form.Item label="Tên ngân hàng" name="bankName">
                  <Input size="large" allowClear />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="Số tài khoản" name="bankNumber">
                  <Input size="large" allowClear />
                </Form.Item>
              </Col>
            </Row>
          </Col>
          <Row className="w-full mb-5">
            <Col span={24}>
              <Row justify="space-between" gutter={[10, 0]} className="mb-5">
                <p className="mb-2 font-bold text-base">Thông tin gia đình</p>
                <Col span={5}>
                  <Button type="primary" className="w-full " onClick={() => setIsFamilyModalVisible(true)}>
                    Thêm thành viên gia đình +
                  </Button>
                </Col>
              </Row>
              <Table columns={columnsFamily} dataSource={familyData} pagination={false} />
            </Col>
          </Row>
          <Row className="w-full">
            <Col span={24}>
              <Row justify="space-between" gutter={[10, 0]} className="mb-5">
                <p className="mb-2 font-bold text-base">Thông tin chứng chỉ</p>
                <Col span={4}>
                  <Button type="primary" className="w-full " onClick={() => setIsCertificateModalVisible(true)}>
                    Thêm chứng chỉ +
                  </Button>
                </Col>
              </Row>
              <Table columns={columnsCertificate} dataSource={certificateData} pagination={false} />
            </Col>
          </Row>
        </Row>
      </Form>
      <Modal
        title="Thành viên gia đình"
        visible={isFamilyModalVisible}
        onOk={handleAddFamilyMember}
        onCancel={() => setIsFamilyModalVisible(false)}
        okText="Thêm"
        cancelText="Hủy"
      >
        <Form form={familyForm} layout="vertical">
          <Row>
            <Col span={24}>
              <Form.Item
                label="Họ và tên"
                name="fullName"
                rules={[{ required: true, message: 'Vui lòng nhập họ và tên' }]}
              >
                <Input />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item
                label="Quan hệ với nhân viên"
                name="relationship"
                rules={[{ required: true, message: 'Vui lòng nhập quan hệ với nhân viên' }]}
              >
                <Input />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label="Ngày sinh"
                name="birthDay"
                rules={[{ required: true, message: 'Vui lòng chọn ngày sinh' }]}
              >
                <DatePicker className="w-full" format="DD/MM/YYYY" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label="Giới tính"
                name="gender"
                rules={[{ required: true, message: 'Vui lòng chọn giới tính' }]}
              >
                <Select>
                  <Select.Option value={true}>Nam</Select.Option>
                  <Select.Option value={false}>Nữ</Select.Option>
                </Select>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Modal>
      <Modal
        title="Thành viên gia đình"
        visible={isCertificateModalVisible}
        onOk={handleAddCertificate}
        onCancel={() => setIsCertificateModalVisible(false)}
        okText="Thêm"
        cancelText="Hủy"
      >
        <Form form={certificateForm} layout="vertical">
          <Row>
            <Col span={24}>
              <Form.Item
                label="Tên chứng chỉ"
                name="nameCertificate"
                rules={[{ required: true, message: 'Vui lòng nhập tên chứng chỉ' }]}
              >
                <Input />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item
                label="Mô tả chứng chỉ"
                name="description"
                rules={[{ required: true, message: 'Vui lòng nhập mô tả chứng chỉr' }]}
              >
                <Input />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Modal>
    </>
  );
};

export default EditUserForm;
