import { UserOutlined } from '@ant-design/icons';
import { Col, DatePicker, DatePickerProps, Form, Input, Row, Select } from 'antd';
import { isEmpty } from 'lodash';
import { IFormProps } from 'model';
import { useState, useEffect } from 'react';
import { useAppSelector } from 'store';
import { selectAppModalForm } from 'store/appSlice';
import { processGetQuery } from 'api';
interface Department {
  id: string;
  departmentName: string;
}
const { Option } = Select;
const UserForm = (props: IFormProps) => {
  const { form } = props;
  const [isLoading, setIsLoading] = useState(false);
  const { editedRow } = useAppSelector(selectAppModalForm);
  const [departments, setDepartments] = useState<Department[]>([]);

  useEffect(() => {
    processGetQuery('/department/getDepartment')
      .then((data) => {
        console.log(data.data);
        setDepartments(data.data);
        setIsLoading(false);
      })
      .catch((error) => {
        console.error('Lỗi khi lấy dữ liệu:', error);
        setIsLoading(false);
      });
  }, []);
  const onChange: DatePickerProps['onChange'] = (date, dateString) => {
    console.log(date);
  };
  return (
    <Form form={form} initialValues={{ remember: true }} labelCol={{ span: 24 }} wrapperCol={{ span: 24 }}>
      <Row gutter={[10, 0]}>
        <Col span={24}>
          <Form.Item
            label="Email"
            name="email"
            rules={[
              { required: true, message: 'Please input your Email!' },
              { type: 'email', message: 'Email is not valid!' },
            ]}
          >
            <Input
              size="large"
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Nhập Email của nhân viên mới"
              allowClear
              disabled={!isEmpty(editedRow)}
            />
          </Form.Item>
        </Col>
        <Col span={24}>
          <Form.Item label="Name" name="name" rules={[{ required: true, message: 'Please input your name!' }]}>
            <Input
              size="large"
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Nhập tên nhân viên mới"
              allowClear
            />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item label="Ngày kí hợp đồng" name="hireDate">
            <DatePicker className="w-full" onChange={onChange} size="large" placeholder="Chọn ngày" />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item label="Chức vụ" name="position">
            <Input size="large" allowClear placeholder="Nhập chức vụ" />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item
            label="Phòng ban"
            name="departmentId"
            rules={[{ required: true, message: 'Please select user role!' }]}
          >
            <Select placeholder="Chọn phòng ban" loading={isLoading} size="large" style={{ width: '100%' }}>
              {departments.map((dept) => (
                <Option key={dept.id} value={dept.id}>
                  {dept.departmentName}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
};

export default UserForm;
