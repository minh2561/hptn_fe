import React, { useState, useEffect } from 'react';
import { Select } from 'antd';
import { processGetQuery } from 'api';

const { Option } = Select;

interface Department {
  id: string;
  departmentName: string;
}

const DepartmentSelect = () => {
  const [departments, setDepartments] = useState<Department[]>([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    processGetQuery('/department/getDepartment')
      .then((data) => {
        console.log(data.data);
        setDepartments(data.data);
        setIsLoading(false);
      })
      .catch((error) => {
        console.error('Lỗi khi lấy dữ liệu:', error);
        setIsLoading(false);
      });
  }, []);

  return (
    <Select placeholder="Chọn phòng ban" loading={isLoading} size="large" style={{ width: '100%' }}>
      {departments.map((dept) => (
        <Option key={dept.id} value={dept.id}>
          {dept.departmentName}
        </Option>
      ))}
    </Select>
  );
};

export default DepartmentSelect;
