import type { TableProps } from 'antd';
import { Button, Col, Row, Table } from 'antd';
import Search from 'antd/es/input/Search';
import AppPagination from 'app/components/common/AppPagination';
import { modalFormConfig } from 'constants/modalForm';
import { EMPLOYEE_STATUS } from 'constants/app';
import { DynamicKeyObject } from 'model';
import { useState } from 'react';
import { modalForm } from 'utils/app';
import DetailUser from './DeitalUser';
import UserAction from 'app/components/custom/UserAction';

interface DataItem {
  name: string;
  position: string;
  departmentName: string;
  districtDomicile: string | null;
  provinceDomicile: string | null;
  [key: string]: any;
}

const User = () => {
  const [data, setData] = useState<DynamicKeyObject>({});
  const [isOpenDetail, setIsOpenDetail] = useState<boolean>(false);
  const [clickedRow, setClickedRow] = useState<DynamicKeyObject>({});
  const role = sessionStorage.getItem('role');
  const handleClickRow = (record: any) => {
    setClickedRow(record);
    setIsOpenDetail(true);
  };
  const columns: TableProps<any>['columns'] = [
    {
      title: 'Tên',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Vị trí',
      dataIndex: 'position',
      key: 'position',
    },
    {
      title: 'Lương',
      dataIndex: 'salary',
      key: 'salary',
    },
    {
      title: 'Mật khẩu',
      dataIndex: 'code',
      key: 'code',
    },
    {
      title: 'Phòng ban',
      render: (_, record) => record.departmentEntity?.departmentName,
      key: 'departmentName',
    },
    {
      title: 'Ngày thuê',
      dataIndex: 'hireDate',
      key: 'hireDate',
    },
    {
      title: 'Trạng thái',
      render: (_, record: DynamicKeyObject) => EMPLOYEE_STATUS[record.typeStatus],
      key: 'typeStatus',
    },
    ...(role == 'ROLE_HR'
      ? [
          {
            title: 'Hành động',
            key: 'action',
            render: (_: any, record: any) => (
              <UserAction
                row={record}
                apiPath={modalFormConfig.editUser.apiPath}
                apiDelete="/employees/deleteEmployee"
              />
            ),
          },
        ]
      : []),
  ];
  const dataSource = data.data;
  console.log('Current data:', dataSource);
  return (
    <>
      <h2 className="uppercase">Quản lý nhân viên </h2>
      <Row justify="end" gutter={[10, 0]} className="mb-5">
        <Col span={4}>
          <Search placeholder="Tìm kiếm" />
        </Col>
        {role === 'ROLE_HR' && (
          <Col span={3}>
            <Button type="primary" className="w-full mb-5" onClick={() => modalForm.open(modalFormConfig.user)}>
              Thêm nhân viên +
            </Button>
          </Col>
        )}
      </Row>
      <Table
        columns={columns}
        dataSource={data.data}
        pagination={false}
        onRow={(record) => ({
          onClick: () => handleClickRow(record),
        })}
      />
      <AppPagination onChangeDataTable={setData} apiPath="/employees/getEmployee" />
      {isOpenDetail && (
        <DetailUser
          isOpen={isOpenDetail}
          handleCloseModal={() => setIsOpenDetail(false)}
          handleSubmitForm={() => setIsOpenDetail(false)}
          row={clickedRow}
        />
      )}
    </>
  );
};

export default User;
