import type { TableProps } from 'antd';
import { Table, Row, Col, Button } from 'antd';
import Search from 'antd/es/input/Search';
import AppPagination from 'app/components/common/AppPagination';
import EditAction from 'app/components/custom/EditAction';
import { modalFormConfig } from 'constants/modalForm';
import { DynamicKeyObject } from 'model';
import { useState } from 'react';
import { modalForm } from 'utils/app';

const Discipline = () => {
  const [data, setData] = useState<DynamicKeyObject>({});
  const role = sessionStorage.getItem('role');
  const columns: TableProps<any>['columns'] = [
    {
      title: 'STT',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'Nguyên nhân kỉ luật',
      dataIndex: 'nameDiscipline',
      key: 'nameDiscipline',
    },
    {
      title: 'Ngày tạo',
      dataIndex: 'createDate',
      key: 'createDate',
    },
    {
      title: 'Tên nhân viên',
      dataIndex: 'employeeName',
      key: 'employeeName',
    },
    ...(role == 'ROLE_HR'
      ? [
          {
            title: 'Hành động',
            key: 'action',
            render: (_: any, record: any) => (
              <EditAction
                row={record}
                apiPath={modalFormConfig.discipline.apiPath}
                apiDelete={'/discipline/delete-discipline'}
              />
            ),
          },
        ]
      : []),
  ];
  console.log(data);
  return (
    <>
      <h2 className="uppercase">Quản lý kỷ luật </h2>
      <Row justify="end" gutter={[10, 0]}>
        <Col span={4}>
          <Search placeholder="Tìm kiếm" />
        </Col>
        {role === 'ROLE_HR' && (
          <Col span={3}>
            <Button type="primary" className="w-full mb-5" onClick={() => modalForm.open(modalFormConfig.discipline)}>
              Thêm kỷ luật +
            </Button>
          </Col>
        )}
      </Row>
      <Table columns={columns} dataSource={data.data} pagination={false} />
      <AppPagination onChangeDataTable={setData} apiPath={'/discipline/getDiscipline'} />
    </>
  );
};

export default Discipline;
