import { Col, Form, Input, Row } from 'antd';

import { IFormProps } from 'model';

const DisciplineForm = (props: IFormProps) => {
  const { form } = props;
  return (
    <Form form={form} initialValues={{ remember: true }} labelCol={{ span: 24 }} wrapperCol={{ span: 24 }}>
      <Row gutter={[10, 0]}>
        <Col span={24}>
          <Form.Item label="Lý do kỷ luật" name="nameDiscipline">
            <Input size="large" placeholder="Nhập lý do kỷ luật" allowClear />
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
};

export default DisciplineForm;
