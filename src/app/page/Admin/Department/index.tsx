import type { TableProps } from 'antd';
import { Button, Col, Row, Table } from 'antd';
import AppPagination from 'app/components/common/AppPagination';
import { DynamicKeyObject } from 'model';
import { useState } from 'react';
import Search from 'antd/es/input/Search';
import { modalForm } from 'utils/app';
import { modalFormConfig } from 'constants/modalForm';

const columns: TableProps<any>['columns'] = [
  {
    title: 'Mã phòng ban',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: 'Tên phòng ban',
    dataIndex: 'departmentName',
    key: 'departmentName',
  },
];

const Department = () => {
  const [data, setData] = useState<DynamicKeyObject>({});
  const role = sessionStorage.getItem('role');

  return (
    <>
      <h2 className="uppercase">Quản lý phòng ban </h2>
      <Row justify="end" gutter={[10, 0]}>
        <Col span={4}>
          <Search placeholder="Tìm kiếm" />
        </Col>
        {role === 'ROLE_HR' && (
          <Col span={3}>
            <Button type="primary" className="w-full mb-5" onClick={() => modalForm.open(modalFormConfig.department)}>
              Thêm phòng ban +
            </Button>
          </Col>
        )}
      </Row>
      <Table columns={columns} dataSource={data.data} pagination={false} />
      <AppPagination onChangeDataTable={setData} apiPath={'/department/getDepartment'} />
    </>
  );
};

export default Department;
