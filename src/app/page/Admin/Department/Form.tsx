import { Col, Form, Input, Row } from 'antd';
import { IFormProps } from 'model';

const DepartmentForm = (props: IFormProps) => {
  const { form } = props;
  return (
    <Form form={form} initialValues={{ remember: true }} labelCol={{ span: 24 }} wrapperCol={{ span: 24 }}>
      <Row gutter={[10, 0]}>
        <Col span={24}>
          <Form.Item label="Tên phòng ban" name="departmentName">
            <Input size="large" placeholder="Nhập tên phòng ban" allowClear />
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
};

export default DepartmentForm;
