import type { TableProps } from 'antd';
import { Table, Row, Col, Button } from 'antd';
import Search from 'antd/es/input/Search';
import AppPagination from 'app/components/common/AppPagination';
import TableAction from 'app/components/custom/TableAction';
import { modalFormConfig } from 'constants/modalForm';
import { DynamicKeyObject } from 'model';
import { useState } from 'react';
import { modalForm } from 'utils/app';

const BenefitMangament = () => {
  const [data, setData] = useState<DynamicKeyObject>({});
  const role = sessionStorage.getItem('role');

  const columns: TableProps<any>['columns'] = [
    {
      title: 'Mã quyền lợi',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'Kiểu quyền lợi',
      dataIndex: 'benefitType',
      key: 'benefitType',
    },
    {
      title: 'Mô tả quyền lợi',
      dataIndex: 'benefitDescription',
      key: 'benefitDescription',
    },
    ...(role == 'ROLE_HR'
      ? [
          {
            title: 'Hành động',
            key: 'action',
            render: (_: any, record: any) => (
              <TableAction
                row={record}
                apiPath={modalFormConfig.benefit.apiPathEdit}
                apiDelete={'/benefit/delete-benefit'}
              />
            ),
          },
        ]
      : []),
  ];
  return (
    <>
      <h2 className="uppercase">Quản lý quyền lợi </h2>
      <Row justify="end" gutter={[10, 0]}>
        <Col span={4}>
          <Search placeholder="Tìm kiếm" />
        </Col>
        {role === 'ROLE_HR' && (
          <Col span={3}>
            <Button type="primary" className="w-full mb-5" onClick={() => modalForm.open(modalFormConfig.benefit)}>
              Thêm quyền lợi +
            </Button>
          </Col>
        )}
      </Row>
      <Table columns={columns} dataSource={data.data} pagination={false} />
      <AppPagination onChangeDataTable={setData} apiPath={'/benefit/get-benefit'} />
    </>
  );
};

export default BenefitMangament;
