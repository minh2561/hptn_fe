import { Col, Form, Input, Row } from 'antd';
import TextArea from 'antd/es/input/TextArea';
import { IFormProps } from 'model';
const BenefitForm = (props: IFormProps) => {
  const { form } = props;

  return (
    <Form form={form} initialValues={{ remember: true }} labelCol={{ span: 24 }} wrapperCol={{ span: 24 }}>
      <Row gutter={[10, 0]}>
        <Col span={24}>
          <Form.Item label="Kiểu quyền lợi" name="benefitType">
            <Input size="large" placeholder="Nhập kiểu quyền lợi" allowClear />
          </Form.Item>
        </Col>
        <Col span={24}>
          <Form.Item label="Mô tả quyền lợi" name="benefitDescription">
            <TextArea size="large" placeholder="Nhập thông tin quyền lợi" allowClear />
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
};

export default BenefitForm;
