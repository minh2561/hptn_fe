import { useState, useEffect } from 'react';
import { Calendar, Badge, ConfigProvider, CalendarProps, BadgeProps, Row, Col, Button } from 'antd';
import type { Dayjs } from 'dayjs';
import dayjs from 'dayjs';
import 'dayjs/locale/vi';
import localeData from 'dayjs/plugin/localeData';
import { loading } from 'utils/app';
import { processGetQuery, processPostQuery } from 'api';

dayjs.extend(localeData);
dayjs.locale('vi');

const CheckinManagement = () => {
  const [selectedDate, setSelectedDate] = useState<Dayjs | null>(dayjs());
  const [checkedIn, setCheckedIn] = useState<boolean>(false);
  const [checkedOut, setCheckedOut] = useState<boolean>(false);
  const [apiData, setApiData] = useState<any[]>([]);
  const token = sessionStorage.getItem('token');
  const [checkInDays, setCheckInDays] = useState<number>(0);
  const [checkOutDays, setCheckOutDays] = useState<number>(0);
  const date = dayjs('2024-07-18');
  useEffect(() => {
    fetchAttendanceData(selectedDate);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedDate, token]);

  const fetchAttendanceData = (date: Dayjs | null) => {
    const formattedDate = date?.format('YYYY-MM-DD');
    const apiUrl = '/time-keeping/get-timeKepping';
    const requestData = {
      time: formattedDate,
    };

    if (formattedDate && token) {
      loading.on();
      processPostQuery(apiUrl, requestData, false, token)
        .then((data) => {
          setApiData(data.data || []);
          const today = dayjs().format('DD/MM/YYYY');
          const isCheckedIn = data.data.some((item: any) => item.checkInDate == today);
          const isCheckedOut = data.data.some((item: any) => item.checkOutDate == today);
          console.log('Ngày vào', today, data.data);
          console.log('giá trị so sánh', isCheckedIn, isCheckedOut);
          setCheckedIn(isCheckedIn);
          setCheckedOut(isCheckedOut);
          calculateCheckInDays(data.data);
          calculateCheckOutDays(data.data);
          loading.off();
        })
        .catch((error) => {
          console.error('Error fetching data:', error);
          loading.off();
        });
    }
  };
  const calculateCheckInDays = (data: any[]) => {
    const workDays = data.filter((item) => item.checkInDate);
    setCheckInDays(workDays.length);
  };
  const calculateCheckOutDays = (data: any[]) => {
    const workDays = data.filter((item) => item.checkOutDate);
    setCheckOutDays(workDays.length);
  };
  const refreshApiData = (date: Dayjs | null) => {
    fetchAttendanceData(date);
  };

  const getListData = (value: Dayjs) => {
    const formattedValue = value.format('DD/MM/YYYY');
    const listData = apiData.reduce((result, item) => {
      if (item.checkInDate === formattedValue && item.checkOutDate === formattedValue) {
        result.push({ type: 'success', content: `${item.checkInTime} - ${item.checkOutTime}` });
      } else if (item.checkInDate === formattedValue) {
        result.push({ type: 'success', content: `${item.checkInTime}` });
      } else if (item.checkOutDate === formattedValue) {
        result.push({ type: 'warning', content: `${item.checkOutTime}` });
      }
      return result;
    }, [] as { type: BadgeProps['status']; content: string }[]);

    return listData;
  };

  const handleCalendarChange = (value: Dayjs) => {
    setSelectedDate(value);
    setCheckedIn(false);
  };

  const handleCheckinClick = () => {
    const apiUrl = '/time-keeping/checkin';
    loading.on();

    if (token) {
      processGetQuery(apiUrl, undefined, token)
        .then((data) => {
          refreshApiData(selectedDate);
          loading.off();
        })
        .catch((error) => {
          console.error('Error fetching data:', error);
          loading.off();
        });
    }
  };

  const handleCheckoutClick = () => {
    const apiUrl = '/time-keeping/checkout';
    loading.on();

    if (token) {
      processGetQuery(apiUrl, undefined, token)
        .then((data) => {
          refreshApiData(selectedDate);
          loading.off();
        })
        .catch((error) => {
          console.error('Error fetching data:', error);
          loading.off();
        });
    }
  };

  const dateCellRender = (value: Dayjs) => {
    const listData = getListData(value);
    return (
      <ul className="events">
        {listData.map((item: any, index: any) => (
          <li key={index}>
            <Badge status={item.type} text={item.content} />
          </li>
        ))}
      </ul>
    );
  };

  const monthCellRender = (value: Dayjs) => {
    return null;
  };

  const cellRender: CalendarProps<Dayjs>['cellRender'] = (current, info) => {
    if (info.type === 'date') return dateCellRender(current);
    if (info.type === 'month') return monthCellRender(current);
    return info.originNode;
  };

  return (
    <>
      <h2 className="uppercase">Quản lý hiện diện</h2>
      <p>Chấm công/Hiện diện</p>
      <Row gutter={[10, 0]} justify="space-around" align="middle" className="mb-4">
        <Col span={4} className="bg-[#f5f5f5] rounded-lg">
          <h4 className="text-center m-0">Tổng số ngày check-in trong tháng</h4>
          <h2 className="text-center m-0">{checkInDays}</h2>
        </Col>
        <Col span={4} className="bg-[#f5f5f5] rounded-lg">
          <h4 className="text-center m-0">Tổng số ngày check-out trong tháng</h4>
          <h2 className="text-center m-0">{checkOutDays}</h2>
        </Col>
      </Row>
      <Row justify="end" gutter={[10, 0]}>
        <Col span={3}>
          {checkedIn === false && (
            <Button
              className="w-full mb-5 rounded-full bg-[#009933] text-white hover:!text-white"
              onClick={handleCheckinClick}
            >
              Check-in
            </Button>
          )}
          {checkedIn === true && checkedOut === false && (
            <Button
              className="w-full mb-5 rounded-full bg-[#009933] text-white hover:!text-white"
              onClick={handleCheckoutClick}
            >
              Check-out
            </Button>
          )}
          {checkedIn === true && checkedOut === true && (
            <Button className="w-full mb-5 rounded-full bg-[#009933] text-white hover:!text-white" disabled>
              Check-out
            </Button>
          )}
        </Col>
      </Row>
      <ConfigProvider locale={{ locale: 'vi' }}>
        <Calendar value={selectedDate || date} cellRender={cellRender} onChange={handleCalendarChange} />
      </ConfigProvider>
    </>
  );
};

export default CheckinManagement;
