import { Calendar, Badge, ConfigProvider, CalendarProps, BadgeProps } from 'antd';
import type { Dayjs } from 'dayjs';
import dayjs from 'dayjs';
import 'dayjs/locale/vi';
import localeData from 'dayjs/plugin/localeData';

dayjs.extend(localeData);
dayjs.locale('vi');

const getListData = (value: Dayjs) => {
  let listData;
  switch (value.date()) {
    case 8:
      listData = [{ type: 'success', content: 'Chấm công thành công.' }];
      break;
    case 15:
      listData = [{ type: 'success', content: 'Chấm công thành công.' }];
      break;
    default:
  }
  return listData || [];
};
const getMonthData = (value: Dayjs) => {
  return null;
};
const monthCellRender = (value: Dayjs) => {
  const num = getMonthData(value);
  return num ? (
    <div className="notes-month">
      <section>{num}</section>
      <span>Backlog number</span>
    </div>
  ) : null;
};

const dateCellRender = (value: Dayjs) => {
  const listData = getListData(value);
  return (
    <ul className="events">
      {listData.map((item) => (
        <li key={item.content}>
          <Badge status={item.type as BadgeProps['status']} text={item.content} />
        </li>
      ))}
    </ul>
  );
};
const cellRender: CalendarProps<Dayjs>['cellRender'] = (current, info) => {
  if (info.type === 'date') return dateCellRender(current);
  if (info.type === 'month') return monthCellRender(current);
  return info.originNode;
};

const CalendarComponent = () => {
  return (
    <ConfigProvider locale={{ locale: 'vi' }}>
      <Calendar cellRender={cellRender} />
    </ConfigProvider>
  );
};

export default CalendarComponent;
