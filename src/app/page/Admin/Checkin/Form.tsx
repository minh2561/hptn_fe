import { Col, Form, Input, Row, DatePicker } from 'antd';
import { IFormProps } from 'model';

const OtForm = (props: IFormProps) => {
  const { form } = props;

  return (
    <Form form={form} initialValues={{ remember: true }} labelCol={{ span: 24 }} wrapperCol={{ span: 24 }}>
      <Row gutter={[10, 0]}>
        <Col span={24}>
          <Form.Item label="Lý do ot" name="reasonOt">
            <Input size="large" allowClear />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item label="Ngày bắt đầu" name="startTime">
            <DatePicker className="w-full" showTime format="YYYY-MM-DD HH:mm:ss" size="large" />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item label="Ngày kết thúc" name="endTime">
            <DatePicker className="w-full" showTime format="YYYY-MM-DD HH:mm:ss" size="large" />
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
};

export default OtForm;
