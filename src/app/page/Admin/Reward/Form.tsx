import { Col, Form, Input, Row, Select } from 'antd';
import { Option } from 'antd/es/mentions';
import { processGetQuery } from 'api';
import { IFormProps } from 'model';
import { useEffect, useState } from 'react';
interface Employee {
  code: string;
  name: string;
}
const RewardForm = (props: IFormProps) => {
  const { form } = props;
  const [employees, setEmployees] = useState<Employee[]>([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    processGetQuery('/employees/getEmployee')
      .then((data) => {
        console.log(data.data);
        setEmployees(data.data);
        setIsLoading(false);
      })
      .catch((error) => {
        console.error('Lỗi khi lấy dữ liệu:', error);
        setIsLoading(false);
      });
  }, []);
  return (
    <Form form={form} initialValues={{ remember: true }} labelCol={{ span: 24 }} wrapperCol={{ span: 24 }}>
      <Row gutter={[10, 0]}>
        <Col span={24}>
          <Form.Item label="Tên phần thưởng" name="nameReward">
            <Input size="large" placeholder="Nhập tên phần thưởng" allowClear />
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
};

export default RewardForm;
function setIsLoading(arg0: boolean) {
  throw new Error('Function not implemented.');
}
