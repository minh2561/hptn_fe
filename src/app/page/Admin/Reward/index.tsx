import type { TableProps } from 'antd';
import { Button, Col, Row, Table } from 'antd';
import AppPagination from 'app/components/common/AppPagination';
import { DynamicKeyObject } from 'model';
import { useState } from 'react';
import Search from 'antd/es/input/Search';
import { modalForm } from 'utils/app';
import { modalFormConfig } from 'constants/modalForm';
import EditAction from 'app/components/custom/EditAction';

const Reward = () => {
  const [data, setData] = useState<DynamicKeyObject>({});
  const role = sessionStorage.getItem('role');
  console.log(role);
  const columns: TableProps<any>['columns'] = [
    {
      title: 'STT',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'Tên phần thưởng',
      dataIndex: 'nameReward',
      key: 'nameReward',
    },
    {
      title: 'Ngày tạo',
      dataIndex: 'createDate',
      key: 'createDate',
    },
    {
      title: 'Tên nhân viên',
      dataIndex: 'employeeName',
      key: 'employeeName',
    },
    ...(role == 'ROLE_HR'
      ? [
          {
            title: 'Hành động',
            key: 'action',
            render: (_: any, record: any) => (
              <EditAction row={record} apiPath={modalFormConfig.reward.apiPath} apiDelete={'/reward/delete-reward'} />
            ),
          },
        ]
      : []),
  ];
  return (
    <>
      <h2 className="uppercase">Quản lý phần thưởng </h2>
      <Row justify="end" gutter={[10, 0]} className="mb-3">
        <Col span={4}>
          <Search placeholder="Tìm kiếm" />
        </Col>
        {role === 'ROLE_HR' && (
          <Col span={3}>
            <Button type="primary" className="w-full mb-5" onClick={() => modalForm.open(modalFormConfig.reward)}>
              Thêm phần thưởng +
            </Button>
          </Col>
        )}
      </Row>
      <Table columns={columns} dataSource={data.data} pagination={false} />
      <AppPagination onChangeDataTable={setData} apiPath={'/reward/getReward'} />
    </>
  );
};

export default Reward;
