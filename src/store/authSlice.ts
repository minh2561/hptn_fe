import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { get } from 'lodash';
import { RootState } from 'store';
import request from 'utils/request';

interface IAuthState {
  user: any;
  accessToken: string;
  role: string;
  isLoading: boolean;
}

const initialState: IAuthState = {
  user: null,
  accessToken: '',
  role: '',
  isLoading: false,
};

export interface IPayloadLogin {
  email: string;
  password: string;
}

export const actionLogin = createAsyncThunk('auth/actionLogin', async (data: IPayloadLogin, { rejectWithValue }) => {
  try {
    return await request({
      url: '/login',
      method: 'POST',
      data,
    });
  } catch (error: any) {
    return rejectWithValue(error);
  }
});

export const slice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    actionLogout(state) {
      sessionStorage.removeItem('token');
      sessionStorage.removeItem('role');
      state.user = null;
      state.accessToken = '';
      state.role = '';
      console.log('Token after logout:', sessionStorage.getItem('token')); // Nên in ra null
      console.log('Role after logout:', sessionStorage.getItem('role'));
    },
    actionUpdateUserLogin(state, action) {
      state.user = action.payload.data.messageCode;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(actionLogin.fulfilled, (state, action) => {
        const { data, messageCode } = get(action, 'payload.data');
        sessionStorage.setItem('token', data.token);
        sessionStorage.setItem('role', data.role);
        state.accessToken = data.token;
        state.user = messageCode;
        state.role = data.role;
        state.isLoading = false;
        console.log('Token after login:', sessionStorage.getItem('token')); 
        console.log('Role after login:', sessionStorage.getItem('role'));
      })
      .addCase(actionLogin.rejected, (state) => {
        state.user = null;
        state.accessToken = '';
        state.role = '';
        state.isLoading = false;
      })
      .addCase(actionLogin.pending, (state) => {
        state.isLoading = true;
      });
  },
});

export const { actionLogout, actionUpdateUserLogin } = slice.actions;
export const selectToken = (state: RootState) => state.auth.accessToken;
export const selectRole = (state: RootState) => state.auth.role;
export const selectUser = (state: RootState) => state.auth.user;
export const selectLoading = (state: RootState) => state.auth.isLoading;

export default slice.reducer;
